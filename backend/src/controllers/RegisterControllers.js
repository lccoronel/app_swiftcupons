const Register = require('../models/Register'); 

module.exports = { 
  async index(request, response) {
    const registers = await Register.find();

    return response.json(registers);
  },

  async store(request, response) {
    const { name, email, manager, token } = request.body;

    const register = await Register.findOne({ name });

    // if (register) {
    //   return response.status(401).json({error: 'Usuario ja existe!'});
    // }
  
    const { _id } = await Register.create({
      name,
      email,
      manager,
      token
    });

    return response.json({ id: _id });
  }
};