const mongoose = require('mongoose');

const RegSchema = new mongoose.Schema({
  name: String,
  email: String,
  manager: Boolean,
  token: String,
});

module.exports = mongoose.model('Register', RegSchema);