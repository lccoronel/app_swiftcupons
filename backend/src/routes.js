const { Router } = require('express');
const RegisterController = require('./controllers/RegisterControllers');

const routes = Router();

routes.post('/register', RegisterController.store);
routes.get('/register', RegisterController.index);

module.exports = routes;